<?php

/**
 * Theme setup
 */
function ws_theme_setup() {

    // setup version and textdomain
    include('includes/WS.php');
    WS::setup('ws', '1.0.0');

    // run includes
    include('includes/customizer.php');

    // include widgets
    require_once('widgets/shortcode-widget.php');

    // woocommerce sensei
    add_theme_support('sensei');
}
add_action('after_setup_theme', 'ws_theme_setup');

/**
 * Helper to get textdomain or version
 *
 * @param string $type textdomain or version. Default textdomain
 * @return string Text domain or version string
 */
function ws($type = 'textdomain') {
    if ('version' == $type) {
        return WS::$version;
    }
    else {
        return WS::$textdomain;
    }
}

/**
 * Get view
 *
 * Short view helper function
 *
 * @param string $view The view
 * @param array $vars View arguments
 * @return string View content
 */
function ws_view($view, $vars = array()) {
    return WS::view($view, $vars);
}

/**
 * Register Widgets
 */
function ws_widgets_init() {

    // include widgets
    require_once('widgets/shortcode-widget.php');

    // register widgets
	register_widget('WS_Shortcode_Widget');
}
add_action('widgets_init', 'ws_widgets_init');

/**
 * Shortcode to show a module based on id
 *
 * @param array $atts Shortcode attributes
 * @return string Shortcode HTML
 */
function ws_divimodule_shortcode($atts) {
    extract(shortcode_atts(array('id' => '*'), $atts));
    return do_shortcode('[et_pb_section global_module="' . $id . '"][/et_pb_section]');
}
add_shortcode('divimodule', 'ws_divimodule_shortcode');

/**
 * Custom Divi modules preparation
 */
function ws_prep_divi_custom_modules() {
    global $pagenow;

    $is_admin = is_admin();
    $action_hook = $is_admin ? 'wp_loaded' : 'wp';
    $required_admin_pages = array('edit.php', 'post.php', 'post-new.php', 'admin.php', 'customize.php', 'edit-tags.php', 'admin-ajax.php', 'export.php'); // list of admin pages where we need to load builder files
    $specific_filter_pages = array('edit.php', 'admin.php', 'edit-tags.php');
    $is_edit_library_page = 'edit.php' === $pagenow && isset($_GET['post_type']) && 'et_pb_layout' === $_GET['post_type'];
    $is_role_editor_page = 'admin.php' === $pagenow && isset($_GET['page']) && 'et_divi_role_editor' === $_GET['page'];
    $is_import_page = 'admin.php' === $pagenow && isset($_GET['import']) && 'wordpress' === $_GET['import'];
    $is_edit_layout_category_page = 'edit-tags.php' === $pagenow && isset($_GET['taxonomy']) && 'layout_category' === $_GET['taxonomy'];

    if (!$is_admin || ($is_admin && in_array($pagenow, $required_admin_pages) && (!in_array($pagenow, $specific_filter_pages) || $is_edit_library_page || $is_role_editor_page || $is_edit_layout_category_page || $is_import_page))) {
        add_action($action_hook, 'ws_divi_custom_modules', 9789);
    }
}
ws_prep_divi_custom_modules();

/**
 * Custom Divi modules
 */
function ws_divi_custom_modules() {
    if (class_exists('ET_Builder_Module')) {
        include('modules/Blog.php');
    }
}

/**
 * Hide Parent Divi theme
 *
 * @param array $themes Available themes
 * @return array Available themes
 */
function ws_theme_hide_parent($themes) {
    unset($themes['Divi']);
    return $themes;
}
add_filter('wp_prepare_themes_for_js', 'ws_theme_hide_parent');


/**
 * Output pagination navigation
 *
 * @param array $args @see paginate_links
 * @param WP_Query $query Optional Query object for pagination. Default to global $wp_query
 */
function ws_page_navi($args = array(), $query = false) {

    // use global query if query not set
    if (!$query) {
        global $wp_query;
        $query = $wp_query;
    }

    // don't display nav for single page
    if ($query->max_num_pages <= 1) {
        return;
    }

    // setup args
    $bignum = 999999999;
    $args = wp_parse_args($args, array(
        'base' => str_replace($bignum, '%#%', esc_url(get_pagenum_link($bignum))),
        'format' => '',
        'current' => max(1, get_query_var('paged')),
        'total' => $query->max_num_pages,
        'prev_text' => '&larr;',
        'next_text' => '&rarr;',
        'type' => 'list',
        'end_size' => 3,
        'mid_size' => 3
    ));

    // output pagination
    echo '<nav class="pagination">';
    echo paginate_links($args);
    echo '</nav>';
}

/**
 * Register post type
 *
 * Helper to add post types with less code
 *
 * @param string $post_type Post type key. Must not exceed 20 characters and may only
 *                          contain lowercase alphanumeric characters, dashes and underscores. See sanitize_key()
 * @param string $singular Post type singular text
 * @param string $plural Post type plural text
 * @param array $args Post type arguments for registration
 * @return WP_Post_Type|WP_Error The registered post type object, or an error object
 */
function ws_register_post_type($post_type, $singular, $plural, $args = array()) {

    // set post type labels
    $labels = array(
        'name' => _x($plural, 'post type general name', 'ws'),
        'singular_name' => _x($singular, 'post type singular name', 'ws'),
        'menu_name' => _x($plural, 'admin menu', 'ws'),
        'name_admin_bar' => _x($singular, 'add new on admin bar', 'ws'),
        'add_new' => _x('Add New', 'hub', 'ws'),
        'add_new_item' => __('Add New ' . $singular, 'ws'),
        'new_item' => __('New ' . $singular, 'ws'),
        'edit_item' => __('Edit ' . $singular, 'ws'),
        'view_item' => __('View ' . $singular, 'ws'),
        'all_items' => __('All ' . $plural, 'ws'),
        'search_items' => __('Search ' . $plural, 'ws'),
        'parent_item_colon' => __('Parent ' . $plural . ':', 'ws'),
        'not_found' => __('No ' . strtolower($plural) . ' found.', 'ws'),
        'not_found_in_trash' => __('No ' . strtolower($plural) . ' found in Trash.', 'ws'),
    );

    // set post type args
    $args = wp_parse_args($args, array(
        'labels' => $labels,
        'description' => $plural,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => $post_type),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail'),
    ));

    // register post type
    return register_post_type($post_type, $args);
}

/**
 * Register taxonomy
 *
 * Helper to add taxonomy with less code
 *
 * @param string $taxonomy Taxonomy key, must not exceed 32 characters
 * @param array|string $object_type Object type or array of object types with which the taxonomy should be associated
 * @param string $singular Taxonomy singular text
 * @param string $plural Taxonomy plural text
 * @param array $args Taxonomy arguments for registration
 * @return void|WP_Error Error object on error, void if success
 */
function ws_register_taxonomy($taxonomy, $object_type, $singular, $plural, $args = array()) {

    // set taxonomy labels
    $labels = array(
        'name' => _x($singular, 'taxonomy general name', 'ws'),
        'singular_name' => _x($singular, 'taxonomy singular name', 'ws'),
        'search_items' => __('Search ' . $plural, 'ws'),
        'popular_items' => __('Popular ' . $plural, 'ws'),
        'all_items' => __('All ' . $plural, 'ws'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Edit ' . $singular, 'ws'),
        'update_item' => __('Update ' . $singular, 'ws'),
        'add_new_item' => __('Add New ' . $singular, 'ws'),
        'new_item_name' => __('New ' . $singular, 'ws'),
        'add_or_remove_items' => __('Add or remove ' . strtolower($plural), 'ws'),
        'choose_from_most_used' => __('Choose from the most used ' . strtolower($plural), 'ws'),
        'not_found' => __('No ' . strtolower($plural) . ' found.', 'ws'),
        'menu_name' => $plural,
    );

    // set taxonomy args
    $args = wp_parse_args($args, array(
        'label' => $plural,
        'labels' => $labels,
        'public' => true,
        'hierarchical' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => $taxonomy),
    ));

    // register taxonomy
    register_taxonomy($taxonomy, $object_type, $args);
}

/**
 * Debug Helpers
 */
if (!function_exists('pr')) {

/**
 * Helper function to output data
 *
 * @param mixed $var Variable to output
 */
function pr($var) {
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}

}

if (!function_exists('prd')) {

/**
 * Helper function to output data and exit
 *
 * @param mixed $var Variable to output
 */
function prd($var) {
    pr($var);
    exit;
}

}

/**
 * Short to test isset() and !empty() functions of a value
 * @param mixed $value
 * @return mixed $value
 */
function ws_isset($value) {
    if (isset($value) && !empty($value)) {
        return $value;
    }
}
