<?php

// run includes
include('ws/divi/functions.php');

/**
 * Set styles and javascript
 */
function ws_enqueue_css_js() {
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('theme-style', get_stylesheet_directory_uri() . '/assets/css/styles.css', array(), ws('version'));
    
    wp_enqueue_script('theme-js', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array('jquery'), ws('version'), true);
}
add_action('wp_enqueue_scripts', 'ws_enqueue_css_js', 99);